import vi from "date-fns/locale/vi";
import "react-datepicker/dist/react-datepicker.css";
import React, { useEffect, useState } from "react";
import Image from "next/image";
import DatePicker, { registerLocale } from "react-datepicker";
registerLocale("vi", vi);
export default function index() {
  const [ngay, setNgay] = useState(new Date());
  const [chitieu, setChitieu] = useState([]);

  const [ghichu, setGhichu] = useState("");

  //Money
  const [sotien, setSotien] = useState(0);
  const [datieu, setDatieu] = useState(0);
  const [conlai, setConlai] = useState(0);
  const [hanmuc, setHanmuc] = useState(0);
  const [trungbinh, setTrungbinh] = useState(0);
  const [ngayconlai, setNgayconlai] = useState(0);

  const formatDate = (date) => {
    let d = new Date(date);
    let month = (d.getMonth() + 1).toString();
    let day = d.getDate().toString();
    let year = d.getFullYear();
    if (month.length < 2) {
      month = "0" + month;
    }
    if (day.length < 2) {
      day = "0" + day;
    }
    return [year, month, day].join("-");
  };

  const list_chitieuHandle = async () => {
    const postData = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        ngay: formatDate(ngay),
      }),
    };
    await fetch("/api/chitieu/list", postData)
      .then(function (response) {
        return response.json();
      })
      .then(function (response) {
        setChitieu(response.chitieu_list.value);
        setDatieu(
          response.chitieu_data.value[0].sotien
            .toString()
            .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")
        );
        setNgayconlai(response.chitieu_data.value[0].songayconlai);
        setTrungbinh(
          response.chitieu_data.value[0].trungbinh
            .toString()
            .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")
        );
        let hanmuc_value = localStorage.getItem("hanmuc") || 0;
        let conlai =
          parseFloat(hanmuc_value.toString().replace(/[.]/g, "")) -
          parseFloat(
            response.chitieu_data.value[0].sotien.toString().replace(/[.]/g, "")
          );
        setConlai(conlai.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));
      });
  };
  const add_chitieuHandle = async () => {
    const postData = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        ngay: formatDate(ngay),
        ghichu: ghichu,
        sotien: sotien,
      }),
    };
    await fetch("/api/chitieu/add", postData)
      .then(function (response) {
        return response.json();
      })
      .then(function (response) {
        list_chitieuHandle();
      });
  };
  const delete_chitieuHandle = async (e, rowid) => {
    const postData = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        rowid: rowid,
      }),
    };
    await fetch("/api/chitieu/delete", postData)
      .then(function (response) {
        return response.json();
      })
      .then(function (response) {
        list_chitieuHandle();
      });
  };
  const change_date = async (e) => {
    await setNgay(e);
    list_chitieuHandle();
  };
  const change_hanmuc = (e) => {
    setHanmuc(e);
    localStorage.setItem("hanmuc", e);
  };

  useEffect(() => {
    if (typeof window !== "undefined") {
      let hanmuc = localStorage.getItem("hanmuc") || "0";
      setHanmuc(hanmuc);
    }

    list_chitieuHandle();
  }, []);
  return (
    <main className="mt-2 mx-2">
      <div className="flex flex-col">
        <div className="flex justify-between">
          <span className="text-green-600 font-semibold">
            Ngày còn lại: {ngayconlai} Ngày
          </span>
          <div>
            <span className="text-green-600 font-semibold">Hạn mức: </span>
            <input
              className="text-green-600 font-semibold w-[80px] text-right"
              value={hanmuc}
              onChange={(e) => change_hanmuc(e.target.value)}
              type="text"
            />
          </div>
        </div>
        <div className="flex justify-between">
          <span className="text-red-500 font-semibold">Đã tiêu: {datieu}</span>
          <span className="text-red-500 font-semibold">Còn lại: {conlai}</span>
        </div>
        <div className="flex justify-between">
          <span className="text-purple-600 font-semibold">
            Trung bình/ngày: {trungbinh}
          </span>
          <DatePicker
            onChange={(date) => change_date(date)}
            id="ngay"
            dateFormat="dd/MM/yyyy"
            locale="vi"
            className="w-20"
            selected={ngay}
          />
        </div>
        <div className="flex justify-start items-center mt-2">
          <input
            onChange={(e) => setGhichu(e.target.value)}
            value={ghichu}
            type="text"
            placeholder="Ghi chú"
            className="w-[100%] border-[1px] p-[5px] rounded-lg mr-6 "
          />
          <input
            onKeyUp={(e) =>
              setSotien(e.target.value.replace(/[^0-9\.\,]/g, ""))
            }
            onChange={(e) => setSotien(e.target.value)}
            value={sotien}
            type="text"
            className="w-20 border-[1px] p-[5px] rounded-lg mr-2"
          />
          <button
            onClick={add_chitieuHandle}
            className="py-[5px] px-6 rounded-lg bg-[darkgreen] text-white"
            type="button"
          >
            Add
          </button>
        </div>
      </div>
      <div className="mt-2">
        <table className="table-fixed">
          <thead>
            <tr>
              <th>Ngày</th>
              <th>Tiêu đề</th>
              <th>Giá</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {chitieu.map((item, key) => {
              return (
                <tr key={key}>
                  <td className="text-center">{item.ngay}</td>
                  <td>{item.ghichu}</td>
                  <td className="text-center">
                    {item.sotien
                      .toString()
                      .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")}
                  </td>
                  <td>
                    <p className="flex justify-center">
                      <Image
                        onClick={(e) => delete_chitieuHandle(e, item.rowid)}
                        src={require("../assets/delete.png")}
                        alt=""
                      />
                    </p>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </main>
  );
}

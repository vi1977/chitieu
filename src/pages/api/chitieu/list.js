import { query } from '../../../lib/db'
export default async function chitieu_list(req, res) {
    var ngay = req.body.ngay;
    const chitieu_list = await query({
        query: `SELECT rowid,strftime('%d/%m',ngay)ngay,sotien,ghichu from thuchi where ngay = "${ngay}" order by rowid`,
        values: []
    })
    const chitieu_data = await query({
        query: `SELECT ifnull(sum(sotien),0)sotien,ifnull(ROUND(sum(sotien)/count(DISTINCT ngay)),0)trungbinh,STRFTIME('%d',DATE('${ngay}','start of month','+1 month','-1 day'))-strftime('%d','${ngay}') as songayconlai
        from thuchi where ngay = "${ngay}" and strftime('%m',ngay) = strftime('%m','${ngay}')`,
        values: []
    })

    return res.status(200).json({
        chitieu_list,
        chitieu_data 
    })
}

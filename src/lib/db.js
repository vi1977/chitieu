const sqlite3 = require('sqlite3').verbose();
export async function query({ query, value = [] }) {
    let db = new sqlite3.Database('data.db', sqlite3.OPEN_READWRITE, async (err) => {
        if (err) {
            return console.error(err.message);
        }
        console.log('Connected success');


    });
    let myPromise = new Promise(function (resolve) {
        db.all(query, async (err, res) => {
            for (let i = 0; i < res.length; i++) {
                value.push(res[i])
            }
        });
        setTimeout(resolve, 500);
    });
    await myPromise;
    return { code: 200, value }
}



// import mysql from 'mysql2/promise'
// export async function query({ query, value = [] }) {
//     const dbconnection = await mysql.createConnection({
//         host: 'localhost',
//         database: 'tpcrm',
//         user: 'root',
//         password: ''
//     })
//     try {
//         const [results] = await dbconnection.execute(query, value);
//         dbconnection.end();
//         return results;
//     } catch (error) {
//     }
// }
